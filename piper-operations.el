;;; PIPER-OPERATIONS --- Unix-like shell functions
;;
;; Author: Howard X. Abrams <howard.abrams@gmail.com>
;; Copyright © 2019, Howard X. Abrams, all rights reserved.
;; Created:  9 October 2019
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Commentary:
;;
;;    Contains a collection of functions that can bbe used interactively through
;;    the Piper UI or as shell-like commands in a piper-script DSL. Each
;;    function purposely mimics a Unix command, like grep, sort, etc. Each
;;    function assumes to work on the entire contents of the current buffer, and
;;    the output will go back into the buffer.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; see the file COPYING.  If not, write to
;; the Free Software Foundation, Inc., 51 Franklin Street, Fifth
;; Floor, Boston, MA 02110-1301, USA.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;; Code:

(require 'dash)
(require 's)

;; I noticed that the `keep-lines' function operates on a region or the
;; buffer from the current location of the point on down. I would like
;; it to operate on the region or the _entire buffer_.
;;
;; The following function is used as _advice_ to new aliases of standard
;; functions that fall into this "don't work with entire buffer" category.

(defun piper-script--start-at-buffer-beginning (&rest ignored)
  (unless (region-active-p)
    (goto-char (point-min))))

(defalias #'piper-script-grep #'keep-lines)
(advice-add #'piper-script-grep :before #'piper-script--start-at-buffer-beginning)

(defalias #'piper-script-grep-v #'flush-lines)
(advice-add #'piper-script-grep-v :before #'piper-script--start-at-buffer-beginning)

(defalias #'piper-script-replace-regexp #'replace-regexp)
(advice-add #'piper-script-replace-regexp :before
            #'piper-script--start-at-buffer-beginning)

(if (functionp 'vr/replace)
    (defalias #'piper-script-replace #'vr/replace)
  (defalias #'piper-script-replace #'replace-regexp))
(advice-add #'piper-script-replace :before
            #'piper-script--start-at-buffer-beginning)

(if (functionp 'vr/query-replace)
    (defalias #'piper-script-query-replace #'vr/query-replace)
  (defalias #'piper-script-query-replace #'query-replace))
(advice-add #'piper-script-query-replace :before
            #'piper-script--start-at-buffer-beginning)

(if (functionp 'spacemacs/sort-lines)
    (defalias #'piper-script-sort #'spacemacs/sort-lines)
  (defun piper-script-sort ()
    "Sort all lines in the current buffer."
    (interactive)
    (sort-lines nil (point-min) (point-max))))

(if (functionp 'spacemacs/sort-lines-reverse)
    (defalias #'piper-script-sort-r #'spacemacs/sort-lines-reverse)
  (defun piper-script-sort-r ()
    "Sort all lines in the current buffer in reverse order."
    (interactive)
    (sort-lines t (point-min) (point-max))))

(if (functionp 'spacemacs/uniquify-lines)
    (defalias #'piper-script-uniq #'spacemacs/uniquify-lines)
  (defun piper-script-uniq ()
    (interactive)
    (message "piper: This function is not implemented yet.")))

(defun piper-script-to-clipboard ()
  "Copies the current buffer to the clipboard's kill ring."
  (interactive)
  (clipboard-kill-ring-save (point-min) (point-max)))

(defun piper-script-lines-to-list (&optional ignore-blanks)
  "Returns the contents of the current buffer as a list where each
element is a line of text. If IGNORE-BLANKS is non-nil, blank lines
are not included in the results."
  (let* ((contents (buffer-substring-no-properties (point-min) (point-max)))
         (lines (s-split "\n" contents)))
    (if ignore-blanks
        (-filter (-compose 'not 's-blank-str?) lines)
      lines)))

(provide 'piper-operations)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; piper-operations.el ends here
